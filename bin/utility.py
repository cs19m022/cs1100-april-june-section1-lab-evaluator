# This file contains paramters used by the projects. These parameters can be modified to change the project behavior.


from cryptography.fernet import Fernet
from subprocess import run, PIPE
import os
import platform
from os.path import dirname, join, abspath
import sys


HOME = abspath(join(dirname(__file__),'..'))
sys.path.append(HOME)
PROJECT_PATH = join(HOME,"project")
TEMPLATE_PATH = join(HOME,"templates")

def compile():
    # Compiling the code, change if needed for other language.
    cmd = f"gcc {PROJECT_PATH}/main.c -o {PROJECT_PATH}/main"
    os.system(cmd)

# Name of executable to generate the outputs for the testcases. (Windows user need to change it to .exe)
current_os = platform.system().lower()
executable = f"{PROJECT_PATH}/main.exe" if current_os=="windows" else f"{PROJECT_PATH}/main"

# Input seperator
ip_sep = '&'


# some utility functions
def call_process(executable,ip):
    try:
        process = run(executable.split(), stdout=PIPE,
                input=ip, encoding='ascii')
    except:
        print(f"{executable} file not found, try compiling the code or check utility.py file contains your command to run the code")        
        exit(-1)
    if(process.returncode!=0):
        raise "Non zero return code"
    return process.stdout

def encrypt(message,passkey):
    encoded_message =  message.encode()
    encoder = Fernet(passkey)
    return encoder.encrypt(encoded_message)


def decrypt(message,passkey):
    decoder = Fernet(passkey)
    return decoder.decrypt(message).decode()
